package com.example.noticiasupt.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.noticiasupt.API.API;
import com.example.noticiasupt.Models.Peticion_Noticia;
import com.example.noticiasupt.Models.Peticion_RNoticia;
import com.example.noticiasupt.R;
import com.example.noticiasupt.Services.ServicioPeticion;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificacionR extends AppCompatActivity {

    ServicioPeticion servicio;
    EditText edtId, edtTitulo, edtDescripcion;
    Button btnCrear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificacion_r);
        servicio = API.getApi(this).create(ServicioPeticion.class);
        edtId = findViewById(R.id.edtId);
        edtTitulo = findViewById(R.id.edtTitulo);
        edtDescripcion = findViewById(R.id.edtDescripcion);
        btnCrear = findViewById(R.id.btnCrear);
        btnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtId.getText().equals("") && edtTitulo.getText().equals("") && edtDescripcion.getText().equals("")) {
                    Toast.makeText(NotificacionR.this, "No puede dejar los campos vacios", Toast.LENGTH_LONG).show();
                }else{
                    CrearNotificacion(edtId.getText().toString(), edtTitulo.getText().toString(), edtDescripcion.getText().toString());
                }
            }
        });
    }

    public void CrearNotificacion(String id, String titulo, String descripcion){
        servicio.CrearNotificacion(id, titulo, descripcion).enqueue(new Callback<Peticion_RNoticia>() {
            @Override
            public void onResponse(Call<Peticion_RNoticia> call, Response<Peticion_RNoticia> response) {
                if(response.isSuccessful()){
                    if(response.body().isEstado()){
                        Toast.makeText(NotificacionR.this, "Notificacion Creada con Exito!!", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(NotificacionR.this, Dashboard.class));
                        finish();
                    }else
                        Toast.makeText(NotificacionR.this, response.body().getDetalle(), Toast.LENGTH_LONG).show();
                }else
                    Toast.makeText(NotificacionR.this, "A Ocurrido Algun error Con los datos", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Peticion_RNoticia> call, Throwable t) {
                Log.e("Error API", t.getCause().toString());
                Log.e("Error API", t.getMessage());
                Log.e("Error API", t.getLocalizedMessage());
                Toast.makeText(NotificacionR.this, "No podemos conectarnos a los servidores Intentelo Mas tarde", Toast.LENGTH_LONG).show();
            }
        });
    }
}
