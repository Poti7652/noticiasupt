package com.example.noticiasupt.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.noticiasupt.R;
import com.example.noticiasupt.Services.FCM;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.http.PUT;

public class NotificacionesPush extends AppCompatActivity {

    Button btnGeneral, btnEspecifico;
    FCM fcm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificaciones_push);
        fcm = new FCM();
        btnGeneral = findViewById(R.id.btnGeneral);
        btnEspecifico = findViewById(R.id.btnEspecifico);

        FirebaseMessaging.getInstance().subscribeToTopic("atodos").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(NotificacionesPush.this, "Se agrego a la Lista", Toast.LENGTH_LONG).show();
            }
        });

        btnEspecifico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Especifico();
            }
        });
        btnGeneral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                General();
            }
        });
    }

    public void General(){
        RequestQueue myRequest   = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try {
            json.put("to", "/topics/"+"atodos");
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo", "atodos");
            notificacion.put("detalle", "atodos");
            json.put("data", notificacion);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, json, null, null){
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> header = new HashMap<>();
                    header.put("content-type", "application/json");
                    header.put("authorization", "key=AAAA3HggxP8:APA91bG8HzCQC7Q-Kh1JTjYIXEGFGavLDKKRT_ggQ9pB7aKbl6L43DBZ1uSQeWRhEnEL7dsoxAhkVrGFflkxh9eVju034i6tyrfT1al9NnwEn3hVE2qtUl3aQqfG9UetaU74ChRBXD3k");
                    return header;
                }
            };
            myRequest.add(request);
        }catch (JSONException e){
            Log.e("JsonExeption", e.toString());
        }
    }
    public void Especifico(){
        RequestQueue myRequest   = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try {
            json.put("to", "eYBjG2uMRCuFAQwBedb1Ex:APA91bGVAXfVgZ7UKNDhtnyleDMeE4CDfACQO8vGhM9td-QtcFLGZ-8zMyynY3I03n-bmIhrVukwqbMg273-hUg7Qoz4ynw4fdtbDq1bmRZdLcBe6QwFAW7lRNG4x-JLtzmZQsiipB3r");
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo", "Prueba de Notificacion");
            notificacion.put("detalle", "Esto solo es una prueba de notificacion");
            json.put("data", notificacion);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, json, null, null){
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> header = new HashMap<>();
                    header.put("content-type", "application/json");
                    header.put("authorization", "key=AAAA3HggxP8:APA91bG8HzCQC7Q-Kh1JTjYIXEGFGavLDKKRT_ggQ9pB7aKbl6L43DBZ1uSQeWRhEnEL7dsoxAhkVrGFflkxh9eVju034i6tyrfT1al9NnwEn3hVE2qtUl3aQqfG9UetaU74ChRBXD3k");
                    return header;
                }
            };
            myRequest.add(request);
        }catch (JSONException e){
            Log.e("JsonExeption", e.toString());
        }
    }
}