package com.example.noticiasupt.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.noticiasupt.API.API;
import com.example.noticiasupt.MainActivity;
import com.example.noticiasupt.Models.Peticion_Noticia;
import com.example.noticiasupt.R;
import com.example.noticiasupt.Services.ServicioPeticion;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Dashboard extends AppCompatActivity {
    ServicioPeticion api;
    Button btnNotificacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        api = API.getApi(Dashboard.this).create(ServicioPeticion.class);
        btnNotificacion = findViewById(R.id.btnNotificacion);
        btnNotificacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, NotificacionesPush.class));
            }
        });
        RecuperarNoticias();
    }

    public void RecuperarNoticias(){
        api.notificaciones().enqueue(new Callback<Peticion_Noticia>() {
            @Override
            public void onResponse(Call<Peticion_Noticia> call, Response<Peticion_Noticia> response) {
                if(response.isSuccessful()){
                    if(response.body().getEstado()){
                        List<Peticion_Noticia.Noticia> detalles = response.body().detalle;
                        for ( Peticion_Noticia.Noticia data: detalles) {
                            Log.i("api", data.getTitulo());
                            Toast.makeText(Dashboard.this, data.getTitulo(), Toast.LENGTH_LONG).show();
                        }
                    }
                }else{
                    Log.e("Errror Conexion:", "La consulta me mando un error");
                    Toast.makeText(Dashboard.this, "A Ocurrido algo en el Servidor", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Peticion_Noticia> call, Throwable t) {
                Log.e("Errror Conexion:", "La consulta me mando un error");
                Toast.makeText(Dashboard.this, "A Ocurrido algo en el Servidor", Toast.LENGTH_LONG).show();
            }
        });
    }
}
