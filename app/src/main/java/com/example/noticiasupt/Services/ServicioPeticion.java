package com.example.noticiasupt.Services;

import com.example.noticiasupt.Models.Peticion_Login;
import com.example.noticiasupt.Models.Peticion_Noticia;
import com.example.noticiasupt.Models.Peticion_RNoticia;
import com.example.noticiasupt.Models.Registro_Usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> registroU(@Field("username") String IDUsuario, @Field("password") String password);

    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login> Sesion(@Field("username") String IDUsuario, @Field("password") String password);

    @GET("api/todasNot")
    Call<Peticion_Noticia> notificaciones();

    @FormUrlEncoded
    @POST("api/crearNotUsuario")
    Call<Peticion_RNoticia> CrearNotificacion(@Field("usuarioId") String Id, @Field("titulo") String titulo, @Field("descripcion") String descripcion);
}
