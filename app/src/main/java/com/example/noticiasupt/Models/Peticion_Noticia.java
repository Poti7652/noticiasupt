package com.example.noticiasupt.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Peticion_Noticia {
    @SerializedName("estado")
    @Expose
    private boolean estado;
    @SerializedName("detalle")
    @Expose
    public List<Noticia> detalle = new ArrayList<>();

    public List<Noticia> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<Noticia> detalle) {
        this.detalle = detalle;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public class Noticia{

        private int id;
        private String titulo;
        private String descripcion;
        private String fecha_creacion;
        private String fecha_actualizacion;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitulo() {
            return titulo;
        }

        public void setTitulo(String titulo) {
            this.titulo = titulo;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public String getFecha_creacion() {
            return fecha_creacion;
        }

        public void setFecha_creacion(String fecha_creacion) {
            this.fecha_creacion = fecha_creacion;
        }

        public String getFecha_actualizacion() {
            return fecha_actualizacion;
        }

        public void setFecha_actualizacion(String fecha_actualizacion) {
            this.fecha_actualizacion = fecha_actualizacion;
        }

    }
}
